package ru.t1.skasabov.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.api.service.IAuthService;
import ru.t1.skasabov.tm.enumerated.Role;
import ru.t1.skasabov.tm.model.User;
import ru.t1.skasabov.tm.util.TerminalUtil;

@NoArgsConstructor
public final class UserRegistryCommand extends AbstractUserCommand {

    @NotNull private static final String NAME = "user-registry";

    @NotNull private static final String DESCRIPTION = "Registry user.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[USER REGISTRY]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        @NotNull final String password = TerminalUtil.nextLine();
        System.out.println("ENTER EMAIL:");
        @NotNull final String email = TerminalUtil.nextLine();
        @NotNull final IAuthService authService = serviceLocator.getAuthService();
        @Nullable final User user = authService.registry(login, password, email);
        showUser(user);
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
